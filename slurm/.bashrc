# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi


__conda_setup="$('/home/software/spack/opt/spack/linux-centos7-x86_64/gcc-9.3.0/miniconda3-4.9.2-fihj225o6lghv6njmb6liqbfwqyiu4gs/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/software/spack/opt/spack/linux-centos7-x86_64/gcc-9.3.0/miniconda3-4.9.2-fihj225o6lghv6njmb6liqbfwqyiu4gs/etc/profile.d/conda.sh" ]; then
        . "/home/software/spack/opt/spack/linux-centos7-x86_64/gcc-9.3.0/miniconda3-4.9.2-fihj225o6lghv6njmb6liqbfwqyiu4gs/etc/profile.d/conda.sh"
    else
        export PATH="/home/software/spack/opt/spack/linux-centos7-x86_64/gcc-9.3.0/miniconda3-4.9.2-fihj225o6lghv6njmb6liqbfwqyiu4gs/bin:$PATH"
    fi
fi

unset __conda_setup

#初始化spack
source /home/software/spack/share/spack/setup-env.sh

#加载日常使用的包，使用spack find可以看到已安装的包
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/home/software/spack/opt/spack/linux-centos7-x86_64/gcc-9.3.0/cuda-11.2.0-xpuf7teu5gma4cluy4azcqybuqfkv63j/extras/CUPTI/lib64"


spack load wget cuda@11.2.0 p7zip@16.02 gcc@9.3.0%gcc@4.8.5 git@2.29.0%gcc@9.3.0 bash cudnn@8.0.4.30-11.1%gcc@9.3.0 singularity@3.8.5 cmake@3.22.2 openblas@0.3.19
# readline@8.1

